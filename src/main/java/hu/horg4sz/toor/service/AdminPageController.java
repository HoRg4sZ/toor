package hu.horg4sz.toor.service;

import hu.horg4sz.toor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mszekely on 10/25/2016.
 */

@Controller
public class AdminPageController {

    private final TeamRepository teamRepository;
    private final MatchRepository matchRepository;
    private boolean teamAddingIsOver;
    private int matchesPerRounds;
    private final TournamentResultRepository tournamentResultRepository;


    @Autowired
    public AdminPageController(TeamRepository teamRepository, MatchRepository matchRepository, TournamentResultRepository tournamentResultRepository) {
        this.teamRepository = teamRepository;
        this.matchRepository = matchRepository;
        this.tournamentResultRepository = tournamentResultRepository;
        matchesPerRounds = 0;
        teamAddingIsOver = false;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET )
    public String loadStartingPage(Model model) {
        List<TournamentResult> all = tournamentResultRepository.findAll();
        List<Team> all1 = teamRepository.findAll();
        TournamentResult tournamentResult = getCurrentTournamentResult();
        List<Match> matches  = tournamentResult.getMatches();
        List<Team> teams = tournamentResult.getTeams();
        teams.sort(Utils::teamComparator);
        model.addAttribute("teams", teams);
        model.addAttribute("matches", matches);
        model.addAttribute("teamAddingIsOver", teamAddingIsOver);
        if (matchesPerRounds != 0) {
            model.addAttribute("matchesPerRounds", matchesPerRounds);
        }
        return "adminPage";
    }

    private TournamentResult getCurrentTournamentResult() {
        if (tournamentResultRepository.findAll().isEmpty()) {
            TournamentResult tournamentResult = new TournamentResult(new ArrayList<Team>(), new ArrayList<Match>());
            tournamentResult.setTournamentDate(LocalDate.now());
            tournamentResultRepository.save(tournamentResult);
            return tournamentResult;
        } else {
            List<TournamentResult> all = tournamentResultRepository.findAll();
            LocalDate now = LocalDate.now();
            List<TournamentResult> toorsWithCurrentDate = tournamentResultRepository.findByTournamentDate(LocalDate.now());
            return tournamentResultRepository.findByTournamentDate(LocalDate.now()).stream().max((o1, o2) -> Long.compare(o1.getId(), o2.getId())).get();
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"add"})
    public String addTeam(HttpServletRequest request) {
        System.out.println(request.getParameter("name"));
        Team team = new Team();
        TournamentResult currentTournamentResult = getCurrentTournamentResult();
        team.setName(request.getParameter("name"));
        team.setTournamentResult(currentTournamentResult);
        teamRepository.save(team);
        currentTournamentResult.getTeams().add(team);
        tournamentResultRepository.save(currentTournamentResult);
        return "redirect:/";
    }
    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"done"})
    public String generateRounds(Model model) {
        teamAddingIsOver = true;
        TournamentResult currentTournamentResult = getCurrentTournamentResult();
        Championship championship = new Championship(currentTournamentResult.getTeams());
        try {
            championship.generateRounds();
        } catch (Exception e) {
            System.out.println("Exception in generating rounds!");
            e.printStackTrace();
        }
        championship.getRounds().forEach(matches -> {
            matches.forEach(match -> {
                match.setTournamentResult(currentTournamentResult);
                matchRepository.save(match);
                currentTournamentResult.getMatches().add(match);
            });
        });

        tournamentResultRepository.save(currentTournamentResult);

        matchesPerRounds = championship.getMatchesPerRounds();

        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"reset"})
    public String reset() {
        TournamentResult tournamentResult = new TournamentResult(new ArrayList<Team>(), new ArrayList<Match>());
        tournamentResult.setTournamentDate(LocalDate.now());
        tournamentResultRepository.save(tournamentResult);
        teamAddingIsOver = false;
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"matchResultSubmit"})
    public String saveMatchResult(HttpServletRequest request) {
        TournamentResult currentTournamentResult = getCurrentTournamentResult();
        List<Match> all = currentTournamentResult.getMatches();

        int matchResultHome;
        int matchResultVisitor;

        for (int i = 0; i < all.size(); i++) {
            String visitorText = request.getParameter("matchResultTextVisitor_" + i);
            String homeText = request.getParameter("matchResultTextHome_" + i);
            boolean homeResIsNumber = org.apache.commons.lang3.math.NumberUtils.isNumber(visitorText);
            boolean visitorResIsnumber = org.apache.commons.lang3.math.NumberUtils.isNumber(homeText);
            Match match = all.get(i);
            if (!visitorText.isEmpty() && !homeText.isEmpty() && homeResIsNumber && visitorResIsnumber) {
                matchResultHome = Integer.parseInt(request.getParameter("matchResultTextHome_" + i));
                matchResultVisitor = Integer.parseInt(visitorText);
                if (match.getHomeGoals() == -1 && match.getVisitorGoals() == -1) {
                    match.getHome().addResult(matchResultHome, matchResultVisitor);
                    match.getVisitor().addResult(matchResultVisitor, matchResultHome);
                } else {
                    match.getHome().modifyResult(matchResultHome, matchResultVisitor, match.getHomeGoals(), match.getVisitorGoals());
                    match.getVisitor().modifyResult(matchResultVisitor, matchResultHome, match.getVisitorGoals(), match.getHomeGoals());
                }
                match.setHomeGoals(matchResultHome);
                match.setVisitorGoals(matchResultVisitor);

            }
        }
        tournamentResultRepository.save(currentTournamentResult);

        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"addMatch"})
    public String addMatch(HttpServletRequest request) {
        TournamentResult currentTournamentResult = getCurrentTournamentResult();
        String teamId1 = request.getParameter("team1");
        String teamId2 = request.getParameter("team2");
        Team team1 = teamRepository.getOne(Long.parseLong(teamId1));
        Team team2 = teamRepository.getOne(Long.parseLong(teamId2));
        Match newMatch = new Match(team1, team2);
        newMatch.setTournamentResult(currentTournamentResult);
        matchRepository.save(newMatch);
        currentTournamentResult.getMatches().add(newMatch);
        tournamentResultRepository.save(currentTournamentResult);
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"addTeamAfterLot"})
    public String addTeamAfterLot(HttpServletRequest request)
    {
        TournamentResult currentTournamentResult = getCurrentTournamentResult();
        String teamName = request.getParameter("name");
        Team newTeam = new Team(teamName);
        newTeam.setTournamentResult(currentTournamentResult);
        teamRepository.save(newTeam);
        currentTournamentResult.getTeams().add(newTeam);
        List<Team> allTeams = currentTournamentResult.getTeams();
        Team newlyAddedTeam = allTeams.get(allTeams.size() - 1);

        allTeams.forEach(team -> {
            if (!team.equals(newlyAddedTeam)) {
                Match newMatch = new Match(team, newlyAddedTeam);
                newMatch.setTournamentResult(currentTournamentResult);
                matchRepository.save(newMatch);
                currentTournamentResult.getMatches().add(newMatch);
            }
        });
        tournamentResultRepository.save(currentTournamentResult);
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, params = {"saveTournament"})
    public String saveTournament() {
        TournamentResult tournamentResult = new TournamentResult(teamRepository.findAll(), matchRepository.findAll());
        tournamentResult.setTournamentDate(LocalDate.now());
        tournamentResultRepository.save(tournamentResult);
        return "redirect:/";
    }

}

