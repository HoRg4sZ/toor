package hu.horg4sz.toor.service;

import hu.horg4sz.toor.model.Team;

/**
 * Created by mszekely on 11/14/2016.
 */
class Utils {
    public static int teamComparator(Team team1, Team team2) {

        int pointCompare = Integer.compare(team2.getPoints(), team1.getPoints());
        if (pointCompare != 0) {
            return pointCompare;
        } else {
            return Integer.compare(team2.getGoalDifference(), team1.getGoalDifference());
        }
    }
}
