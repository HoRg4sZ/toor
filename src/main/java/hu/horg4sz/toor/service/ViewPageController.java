package hu.horg4sz.toor.service;

import hu.horg4sz.toor.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mszekely on 11/14/2016.
 */

@Controller
public class ViewPageController {


    private final TeamRepository teamRepository;
    private final MatchRepository matchRepository;
    private final TournamentResultRepository tournamentResultRepository;
    private long selectedTournamentNumber;

    @Autowired
    public ViewPageController(TeamRepository teamRepository, MatchRepository matchRepository, TournamentResultRepository tournamentResultRepository) {
        this.teamRepository = teamRepository;
        this.matchRepository = matchRepository;
        this.tournamentResultRepository = tournamentResultRepository;
        selectedTournamentNumber = -1;
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String loadMainView(Model model) {

        TournamentResult selectedTournament = tournamentResultRepository.getOne(selectedTournamentNumber);

        if (selectedTournamentNumber != -1) {
            model.addAttribute("prevTeams", selectedTournament.getTeams());
            model.addAttribute("prevMatches", selectedTournament.getMatches());
        }
        TournamentResult tournamentResult = tournamentResultRepository.findByTournamentDate(LocalDate.now()).stream().max((o1, o2) -> Long.compare(o1.getId(), o2.getId())).get();
        List<Team> teams = tournamentResult.getTeams();
        List<Match> matches = tournamentResult.getMatches();
        List<TournamentResult> previousTournaments = tournamentResultRepository.findAll();

        teams.sort(Utils::teamComparator);
        model.addAttribute("teams", teams);
        model.addAttribute("matches", matches);
        model.addAttribute("previousTournaments", previousTournaments);

        int matchesPerRounds = teams.size() / 2;

        if (matchesPerRounds != 0) {
            model.addAttribute("matchesPerRounds", matchesPerRounds);
        }
        return "view";
    }

    @RequestMapping(value = "/view", method = RequestMethod.POST, params = {"selectTournament"})
    public String loadPreviousTournament(HttpServletRequest request) {
        selectedTournamentNumber = Long.parseLong(request.getParameter("tournament"));
        return "redirect:/view";
    }
}
