package hu.horg4sz.toor.service;

import hu.horg4sz.toor.model.Match;
import hu.horg4sz.toor.model.Team;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mszekely on 10/26/2016.
 */

public class Championship {



    private List<List<Match>> rounds;
    private List<Team> teams;

    private int matchesPerRounds;

    public Championship() {

    }

    public Championship(List<Team> teams) {
        this.teams = teams;
    }

    public void generateRounds() throws Exception {
        if (teams.size() > 8) {
            rounds = new ArrayList<>();
            List<Match> round = generateMatches();
            rounds.add(round);
            matchesPerRounds = teams.size() / 2;
        } else {
            boolean phantom = false;
            if (teams.size() % 2 != 0) {
                teams.add(new Team("phantom"));
                phantom = true;
            }
            List<Match> matches = generateMatches();
            int numberOfTeams = teams.size();

            matchesPerRounds = numberOfTeams / 2;
            rounds = new ArrayList<>();
            while(!matches.isEmpty()) {
                List<Match> round = new ArrayList<>();
                for (int i = 0; i < matchesPerRounds; i++) {
                    addMatchToRound(round, matches);
                }
                rounds.add(round);
            }
            if (phantom) {
                for (List<Match> round:rounds) {
                    round.removeIf(roundMatch -> roundMatch.getHome().getName().equals("phantom") || roundMatch.getVisitor().getName().equals("phantom"));
                }
            }
        }


    }

    private void addMatchToRound(List<Match> round, List<Match> matches) throws Exception {

        Match validMatch = null;
        for (int i = 0; i < matches.size(); i++) {
            Match match;
            if (round.size() % 2 == 0) {
                match = matches.get(i);
            } else {
                match = matches.get(matches.size() - i - 1);
            }
                if (isMatchValid(round, match)) {
                    validMatch = match;
                }
        }

        /*Match validMatch = matches.stream().filter(match -> {
            return isMatchValid(round, match);
        }).findFirst().get();*/
        if (validMatch == null) {
            throw new Exception();
        }


        round.add(validMatch);
        matches.remove(validMatch);
        System.out.println("----------------------");
        for (Match match : round) {
            if (match != null) {
                System.out.println(match.getHome().getName() + match.getVisitor().getName());
            }
        }
    }

    private boolean isMatchValid(List<Match> round, Match match) {

        for (Match roundMatch : round) {
            if (roundMatch != null) {
                if (roundMatch.getHome().equals(match.getHome())
                        || roundMatch.getHome().equals(match.getVisitor())
                        || roundMatch.getVisitor().equals(match.getHome())
                        || roundMatch.getVisitor().equals(match.getVisitor())) {
                    return false;
                }
            }
        }
        if (round.isEmpty()) {
            /*boolean wasThisMatchAFirstMatchBefore = rounds.stream()
                    .map(matches -> matches.get(0))
                    .anyMatch(matchBefore -> {
                        if(matchBefore.getHome().equals(match.getHome())
                            || matchBefore.getHome().equals(match.getVisitor())
                            || matchBefore.getVisitor().equals(match.getHome())
                            || matchBefore.getVisitor().equals(match.getVisitor())) {
                            return true;
                        } else {
                            return false;
                        }
                    });
            return !wasThisMatchAFirstMatchBefore;*/

        } else {
            if (teams.size() > 4 && !rounds.isEmpty() && (round.size() < matchesPerRounds - 1 || matchesPerRounds == 2)) {
                List<List<Team>> previousTeamCompositions = new ArrayList<>();
                List<Team> currentRoundTeamComposition = new ArrayList<>();
                for (List<Match> matchesInRound : rounds) {
                    List<Team> teamsInComposition = new ArrayList<>();
                    for (int j = 0; j < matchesInRound.size() - 1; j++) {
                        Match matchInRound = matchesInRound.get(j);
                        teamsInComposition.add(matchInRound.getHome());
                        teamsInComposition.add(matchInRound.getVisitor());
                    }
                    if (matchesPerRounds == 2) {
                        teamsInComposition.add(matchesInRound.get(1).getHome());
                        teamsInComposition.add(matchesInRound.get(1).getVisitor());
                    }
                    previousTeamCompositions.add(teamsInComposition.stream().sorted((t1, t2) -> (t1.getName().compareTo(t2.getName()))).collect(Collectors.toList()));
                }
                for (Match currentRoundMatch: round) {
                    currentRoundTeamComposition.add(currentRoundMatch.getHome());
                    currentRoundTeamComposition.add(currentRoundMatch.getVisitor());
                }
                currentRoundTeamComposition.add(match.getHome());
                currentRoundTeamComposition.add(match.getVisitor());
                currentRoundTeamComposition = currentRoundTeamComposition.stream().sorted((t1,t2) -> (t1.getName().compareTo(t2.getName()))).collect(Collectors.toList());

                for (List<Team> previousTeamComposition: previousTeamCompositions) {
                    if (previousTeamComposition.equals(currentRoundTeamComposition)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public List<Match> generateMatches() {
        List<Match> matches = new ArrayList<>();
        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1 ; j < teams.size(); j++) {
                matches.add(new Match(teams.get(i),teams.get(j)));
            }
        }
        return matches;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public List<List<Match>> getRounds() {
        return rounds;
    }

    public void setRounds(List<List<Match>> rounds) {
        this.rounds = rounds;
    }

    public int getMatchesPerRounds() {
        return matchesPerRounds;
    }


}
