package hu.horg4sz.toor.model;

import hu.horg4sz.toor.service.Championship;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mszekely on 10/25/2016.
 */
@Entity
public class Team {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private int goalsScored;
    private int goalsConceded;
    private int goalDifference;
    private int points;
    private int numberOfVictories;

    @OneToMany(mappedBy = "home")
    private List<Match> homeMatches;

    @OneToMany(mappedBy = "visitor")
    private List<Match> visitorMatches;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "championship_id")
    private TournamentResult tournamentResult;

    public Team() {
        goalsScored = 0;
        goalsConceded = 0;
        points = 0;
        numberOfVictories = 0;
        goalDifference = 0;
    }

    public void addResult(int myGoals, int opponentsGoals) {
        goalsScored += myGoals;
        goalsConceded += opponentsGoals;
        int goalDiffFromCurrentMatch = myGoals - opponentsGoals;
        goalDifference += goalDiffFromCurrentMatch;
        if (goalDiffFromCurrentMatch == 0) {
            points++;
        } else if (goalDiffFromCurrentMatch > 0) {
            points += 3;
            numberOfVictories++;
        }
    }

    public void modifyResult(int myGoals, int opponentsGoals, int myPrevGoalsOnThisMatch, int opponentsPrevGoalsOnThisMatch) {
        goalsScored += myGoals - myPrevGoalsOnThisMatch;
        goalsConceded += opponentsGoals - opponentsPrevGoalsOnThisMatch;
        int goalDifferenceFromCurrentMatch = myGoals - opponentsGoals;
        int goalDifferenceFromPrev = myPrevGoalsOnThisMatch - opponentsPrevGoalsOnThisMatch;
        goalDifference += goalDifferenceFromCurrentMatch - goalDifferenceFromPrev;
        if (goalDifferenceFromPrev < 0) {
            if (goalDifferenceFromCurrentMatch > 0) {
                points += 3;
                numberOfVictories++;
            } else if (goalDifferenceFromCurrentMatch == 0) {
                points++;
            }
        } else if (goalDifferenceFromPrev == 0) {
            if (goalDifferenceFromCurrentMatch > 0) {
                points += 2;
                numberOfVictories++;
            } else if (goalDifferenceFromCurrentMatch < 0) {
                points--;
            }
        } else if (goalDifferenceFromPrev > 0) {
            if (goalDifferenceFromCurrentMatch == 0) {
                points -= 2;
                numberOfVictories--;
            } else if (goalDifferenceFromCurrentMatch < 0){
                points -= 3;
                numberOfVictories--;
            }
        }

    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else {
            Team otherTeam = (Team) o;
            return this.getName().equals(otherTeam.getName());
        }
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + goalsScored;
        result = 31 * result + goalsConceded;
        result = 31 * result + goalDifference;
        result = 31 * result + points;
        result = 31 * result + numberOfVictories;
        result = 31 * result + homeMatches.hashCode();
        result = 31 * result + visitorMatches.hashCode();
        return result;
    }

    public Team(String name) {
        this.name = name;
        goalsScored = 0;
        goalsConceded = 0;
        points = 0;
        numberOfVictories = 0;
        goalDifference = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public void setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
    }

    public int getGoalsConceded() {
        return goalsConceded;
    }

    public void setGoalsConceded(int goalsConceded) {
        this.goalsConceded = goalsConceded;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getNumberOfVictories() {
        return numberOfVictories;
    }

    public void setNumberOfVictories(int numberOfVictories) {
        this.numberOfVictories = numberOfVictories;
    }

    public List<Match> getHomeMatches() {
        return homeMatches;
    }

    public void setHomeMatches(List<Match> homeMatches) {
        this.homeMatches = homeMatches;
    }

    public List<Match> getVisitorMatches() {
        return visitorMatches;
    }

    public void setVisitorMatches(List<Match> visitorMatches) {
        this.visitorMatches = visitorMatches;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }


    public TournamentResult getTournamentResult() {
        return tournamentResult;
    }

    public void setTournamentResult(TournamentResult tournamentResult) {
        this.tournamentResult = tournamentResult;
    }
}
