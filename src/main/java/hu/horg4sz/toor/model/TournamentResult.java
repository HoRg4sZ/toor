package hu.horg4sz.toor.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by mszekely on 11/22/2016.
 */
@Entity
public class TournamentResult {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private LocalDate tournamentDate;

    @OneToMany(mappedBy = "tournamentResult")
    private List<Team> teams;

    @OneToMany(mappedBy = "tournamentResult")
    private List<Match> matches;

    public TournamentResult(List<Team> teams, List<Match> matches) {
        this.teams = teams;
        this.matches = matches;
    }

    public TournamentResult(){

    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public LocalDate getTournamentDate() {
        return tournamentDate;
    }

    public void setTournamentDate(LocalDate tournamentDate) {
        this.tournamentDate = tournamentDate;
    }

    public Long getId() {
        return id;
    }
}
