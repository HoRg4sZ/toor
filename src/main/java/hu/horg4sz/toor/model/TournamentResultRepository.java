package hu.horg4sz.toor.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by mszekely on 11/22/2016.
 */
public interface TournamentResultRepository extends JpaRepository<TournamentResult, Long> {

    List<TournamentResult> findByTournamentDate(LocalDate tournamentDate);

}
