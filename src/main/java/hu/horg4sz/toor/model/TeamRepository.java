package hu.horg4sz.toor.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by mszekely on 10/25/2016.
 */
public interface TeamRepository extends JpaRepository<Team, Long> {

    List<Team> findByName(String name);

}
