package hu.horg4sz.toor.model;

import hu.horg4sz.toor.service.Championship;

import javax.persistence.*;

/**
 * Created by mszekely on 10/26/2016.
 */
@Entity
public class Match {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "home_team_id")
    private Team home;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "visitor_team_id")
    private Team visitor;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "championship_id")
    private TournamentResult tournamentResult;

    private int homeGoals;
    private int visitorGoals;

    public Match() {
        homeGoals = -1;
        visitorGoals = -1;
    }



    public Match(Team home, Team visitor) {
        this.home = home;
        this.visitor = visitor;
        homeGoals = -1;
        visitorGoals = -1;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        if (homeGoals != match.homeGoals) return false;
        if (visitorGoals != match.visitorGoals) return false;

        if (!home.equals(match.home)) return false;
        return visitor.equals(match.visitor);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + home.hashCode();
        result = 31 * result + visitor.hashCode();
        result = 31 * result + homeGoals;
        result = 31 * result + visitorGoals;
        return result;
    }

    public Team getHome() {
        return home;
    }

    @Override
    public String toString() {
        return home.getName() + " - " + visitor.getName() ;
    }

    public void setHome(Team home) {
        this.home = home;
    }

    public Team getVisitor() {
        return visitor;
    }

    public void setVisitor(Team visitor) {
        this.visitor = visitor;
    }

    public int getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(int homeGoals) {
        this.homeGoals = homeGoals;
    }

    public int getVisitorGoals() {
        return visitorGoals;
    }

    public void setVisitorGoals(int visitorGoals) {
        this.visitorGoals = visitorGoals;
    }


    public TournamentResult getTournamentResult() {
        return tournamentResult;
    }

    public void setTournamentResult(TournamentResult tournamentResult) {
        this.tournamentResult = tournamentResult;
    }
}
