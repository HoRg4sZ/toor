package hu.horg4sz.toor.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by mszekely on 11/8/2016.
 */
public interface MatchRepository extends JpaRepository<Match, Long> {

    List<Match> findByHomeAndVisitor(Team home, Team visitor);

}
