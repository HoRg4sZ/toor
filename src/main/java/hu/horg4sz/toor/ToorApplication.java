package hu.horg4sz.toor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToorApplication.class, args);
	}
}
