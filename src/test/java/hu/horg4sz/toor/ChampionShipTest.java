package hu.horg4sz.toor;

import hu.horg4sz.toor.service.Championship;
import hu.horg4sz.toor.model.Match;
import hu.horg4sz.toor.model.Team;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mszekely on 10/26/2016.
 */
public class ChampionShipTest {

    private Championship championship;

    @Before
    public void setUp() {
        List<Team> teams = new ArrayList<>();
        teams.add(new Team("A"));
        teams.add(new Team("B"));
        teams.add(new Team("C"));
        teams.add(new Team("D"));
        teams.add(new Team("E"));
        teams.add(new Team("F"));
        teams.add(new Team("G"));
        teams.add(new Team("H"));
        this.championship = new Championship(teams);
    }

    @Test
    public void generateMatchesTest() {
        List<String> matches = Arrays.asList(
                "AB",
                "AC",
                "AD",
                "BC",
                "BD",
                "CD"
        );
        List<String> generatedMatches = championship.generateMatches().stream()
                .map(match -> match.getHome().getName() + match.getVisitor().getName()).collect(Collectors.toList());
        generatedMatches.forEach(System.out::println);
        /*assertEquals(matches, generatedMatches);*/
    }

    @Test
    public void generateRoundsTest() {

        String reference = "ABCD|ACBD|ADBC|";

        try {
            championship.generateRounds();
        } catch (Exception e) {
            System.out.println("Baj volt!");
            e.printStackTrace();
        }
        StringBuilder test = new StringBuilder();
        championship.getRounds().forEach(matches -> {
            for (Match matche : matches) {
                test.append(matche.getHome().getName() + matche.getVisitor().getName());
            }
            test.append("|");
        });
        System.out.println(test.toString());
        /*assertEquals(reference, test.toString());*/
    }
}
